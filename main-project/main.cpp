#include <iostream>
#include <iomanip>
#include <locale>
#include "file_reader.h"
#include "constants.h"
#include "precipitation1.h"

using namespace std;

bool compareByAmount(const Precipitation* a, const Precipitation* b) {
    return a->amount < b->amount;
}

bool compareByTypeDate(const Precipitation* a, const Precipitation* b) {
    if (a->type != b->type) {
        return a->type < b->type;
    }
    if (a->start.month != b->start.month) {
        return a->start.month < b->start.month;
    }
    return a->start.day < b->start.day;
}

void shakerSort(Precipitation* arr[], int n, bool (*compare)(const Precipitation*, const Precipitation*)) {
    bool swapped = true;
    int start = 0;
    int end = n - 1;

    while (swapped) {
        swapped = false;

        for (int i = start; i < end; ++i) {
            if (!compare(arr[i], arr[i + 1])) {
                swap(arr[i], arr[i + 1]);
                swapped = true;
            }
        }

        if (!swapped)
            break;

        swapped = false;
        --end;

        for (int i = end - 1; i >= start; --i) {
            if (!compare(arr[i], arr[i + 1])) {
                swap(arr[i], arr[i + 1]);
                swapped = true;
            }
        }

        ++start;
    }
}

void quickSort(Precipitation* arr[], int low, int high, bool (*compare)(const Precipitation*, const Precipitation*)) {
    if (low < high) {
        Precipitation* pivot = arr[high];
        int i = low - 1;

        for (int j = low; j <= high - 1; ++j) {
            if (compare(arr[j], pivot)) {
                ++i;
                swap(arr[i], arr[j]);
            }
        }
        swap(arr[i + 1], arr[high]);
        int pi = i + 1;

        quickSort(arr, low, pi - 1, compare);
        quickSort(arr, pi + 1, high, compare);
    }
}

int main() {
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �4. GIT\n";
    cout << "������� �3. ������\n";
    cout << "�����: ������ ����������\n\n";
    cout << "������: 23��1�\n";
    Precipitation* precipitations[MAX_FILE_ROWS_COUNT];
    int size;

    try {
        read("data.txt", precipitations, size);

        cout << "*** ������ ***\n\n";
        for (int i = 0; i < size; i++) {
            cout << "���� ������.....: ";
            cout << setw(2) << setfill('0') << precipitations[i]->start.day << '\n';
            cout << "����� ������....: ";
            cout << setw(2) << setfill('0') << precipitations[i]->start.month << '\n';
            cout << "���� �����......: ";
            cout << setw(2) << setfill('0') << precipitations[i]->end.day << '\n';
            cout << "����� �����.....: ";
            cout << setw(2) << setfill('0') << precipitations[i]->end.month << '\n';
            cout << "����������......: " << fixed << setprecision(2) << precipitations[i]->amount << '\n';
            cout << "���.............: " << precipitations[i]->type << '\n';
        }

        shakerSort(precipitations, size, compareByAmount);
        cout << "\n*** ������ ����� ���������� �� ���������� ***\n\n";
        for (int i = 0; i < size; i++) {
            cout << "���� ������.....: ";
            cout << setw(2) << setfill('0') << precipitations[i]->start.day << '\n';
            cout << "����� ������....: ";
            cout << setw(2) << setfill('0') << precipitations[i]->start.month << '\n';
            cout << "���� �����......: ";
            cout << setw(2) << setfill('0') << precipitations[i]->end.day << '\n';
            cout << "����� �����.....: ";
            cout << setw(2) << setfill('0') << precipitations[i]->end.month << '\n';
            cout << "����������......: " << fixed << setprecision(2) << precipitations[i]->amount << '\n';
            cout << "���.............: " << precipitations[i]->type << '\n';
        }

        quickSort(precipitations, 0, size - 1, compareByTypeDate);
        cout << "\n*** ������ ����� ���������� �� ����, ������ � ��� ***\n\n";
        for (int i = 0; i < size; i++) {
            cout << "���� ������.....: ";
            cout << setw(2) << setfill('0') << precipitations[i]->start.day << '\n';
            cout << "����� ������....: ";
            cout << setw(2) << setfill('0') << precipitations[i]->start.month << '\n';
            cout << "���� �����......: ";
            cout << setw(2) << setfill('0') << precipitations[i]->end.day << '\n';
            cout << "����� �����.....: ";
            cout << setw(2) << setfill('0') << precipitations[i]->end.month << '\n';
            cout << "����������......: " << fixed << setprecision(2) << precipitations[i]->amount << '\n';
            cout << "���.............: " << precipitations[i]->type << '\n';
        }

        for (int i = 0; i < size; i++) {
            delete precipitations[i];
        }
    }
    catch (const char* error) {
        cout << error << '\n';
    }

    return 0;
}
