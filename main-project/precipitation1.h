#pragma once
#ifndef PRECIPITATION1_H
#define PRECIPITATION1_H

#include <string>

using namespace std;

struct Date {
    int day;
    int month;
};

struct Precipitation {
    Date start;
    Date end;
    double amount;
    string type;  // ���������� ���� type
};

#endif // PRECIPITATION1_H
