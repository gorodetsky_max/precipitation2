#pragma once
#ifndef FILTER_H
#define FILTER_H

#include "precipitation1.h"

Precipitation** filter(Precipitation* array[], int size, bool (*check)(Precipitation* element), int& result_size);

/*
  filter:
    ��������� ������ ������� �� ��������� �������, ���������� ����� ������,
    ���������� ������ ��������, ��������������� �������.

  ���������:
    array       - ������ �������
    size        - ������ �������
    check       - �������, ������������ ������� ����������

  ����������:
    ����� ������ � ����������, ���������������� �������.
    result_size - ������ ������ �������
*/

bool check_precipitation_by_rain(Precipitation* element);

/*
  check_precipitation_by_rain:
    ���������, ��� �� �����.

  ���������:
    element - ������� �������

  ����������:
    true, ���� ��� �����, ����� false
*/

bool check_precipitation_by_amount(Precipitation* element);

/*
  check_precipitation_by_amount:
    ���������, ��� �� ����� ������� ������ 1.5.

  ���������:
    element - ������� �������

  ����������:
    true, ���� ����� ������� ������ 1.5, ����� false
*/

#endif // FILTER_H

