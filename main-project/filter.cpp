#include "filter.h"

Precipitation** filter(Precipitation* array[], int size, bool (*check)(Precipitation* element), int& result_size) {
    Precipitation** temp = new Precipitation * [size];
    result_size = 0;

    for (int i = 0; i < size; ++i) {
        if (check(array[i])) {
            temp[result_size++] = array[i];
        }
    }

    Precipitation** result = new Precipitation * [result_size];
    for (int i = 0; i < result_size; ++i) {
        result[i] = temp[i];
    }

    delete[] temp;
    return result;
}

bool check_precipitation_by_rain(Precipitation* element) {
    return element->type == "rain";
}

bool check_precipitation_by_amount(Precipitation* element) {
    return element->amount < 1.5;
}
