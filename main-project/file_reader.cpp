#include "precipitation1.h"
#include "file_reader.h"
#include "constants.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>


using namespace std;

void read(const char* filename, Precipitation* precipitations[], int& size) {
    ifstream file(filename);
    if (!file.is_open()) {
        throw "�� ������� ������� ����.";
    }

    string line;
    size = 0;

    while (getline(file, line) && size < MAX_FILE_ROWS_COUNT) {
        istringstream iss(line);
        Precipitation* p = new Precipitation;

        string day, month, amount, type;
        if (getline(iss, day, ',') && getline(iss, month, ',') && getline(iss, amount, ',') && getline(iss, type, ',')) {
            p->start.day = stoi(day);
            p->start.month = stoi(month);
            p->end.day = p->start.day;  // ������������, ��� ���� ��������� ��������� � ���� ������
            p->end.month = p->start.month;  // ������������, ��� ����� ��������� ��������� � ������� ������
            p->amount = stod(amount);
            p->type = type;
        }
        else {
            delete p;
            throw "������ ������ ������ �� ������.";
        }

        precipitations[size++] = p;
    }

    file.close();
}
