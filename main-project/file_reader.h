#ifndef FILE_READER_H
#define FILE_READER_H

#include "precipitation1.h"

void read(const char* filename, Precipitation* precipitations[], int& size);

#endif // FILE_READER_H
